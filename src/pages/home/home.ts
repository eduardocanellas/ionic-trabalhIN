import { LoginPage } from './../login/login';
import { SesionsProvider } from './../../providers/sesions/sesions';
import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  latitude: number;
  longitude: number;
  textoLat: string;
  textoLong: string;

  userName: string = "";

  constructor(
    public navCtrl: NavController, 
    public sessions: SesionsProvider,
    public app: App,
    public geo: Geolocation)
  {
    this.textoLat = `Sua latitude é Desconhecida!`;
    this.textoLong = `Sua longitude é Desconhecida!`;
  }

  ionViewWillEnter(){
    let user = localStorage.getItem("user");
    this.userName = JSON.parse(user).name;
  }

  getGeoPosition(){
    this.textoLat = `Carregando Latitude...`;
    this.textoLong = `Carregando Longitude...`;

    this.geo.getCurrentPosition().then( 
      (data) =>
      {
        this.latitude = data.coords.latitude;
        this.longitude = data.coords.longitude;

        this.textoLat = `Latitude: ${this.latitude}`;
        this.textoLong = `Longitude: ${this.longitude}`;
      }
    ).catch(
      (error) => 
      {
        console.log(error.json());
      }
    );
  }

  logout(): void{
    this.sessions.logout();
    this.app.getRootNav().push(LoginPage);
  }
}
